
//This code is from ChatGPT. I've made some changes but mainly it's from there. 
//I asked the code one component at the time, so it shouldn't be a direct copy of someone else's code.

#include <MPU6050.h>
#include <Wire.h>
const int MPU_ADDR = 0x68;
int16_t accelerometer_x, accelerometer_y, accelerometer_z;
int16_t gyro_x, gyro_y, gyro_z;
MPU6050 mpu;

int digitPins[] = {2, 3, 4, 5}; // pins connected to the digit select pins of the display
int segmentPins[] = {6, 7, 8, 9, 10, 11, 12, 13}; // pins connected to the segments of the display
int commonPin = 3;
int hourButton = 15;
int minuteButton = 16;
int alarmButton = 17;
int motorPin = 20;
int minute;
int hour;
int alarmHours = 0;
int alarmMinutes = 1;

// Define the segment patterns for each digit (0-9)
byte segments[] = {
  B11000000, // 0
  B11111001, // 1 
  B10100100, // 2 
  B10110000, // 3 
  B10011001, // 4 
  B10010010, // 5 
  B10000010, // 6
  B11111000, // 7
  B10000000, // 8
  B10010000  // 9 
};

void setup() {
  // Set all digit and segment pins as outputs
  for (int i = 0; i < 4; i++) {
    pinMode(digitPins[i], OUTPUT);
  }
  for (int i = 0; i < 8; i++) {
    pinMode(segmentPins[i], OUTPUT);
  }

  pinMode(hourButton, INPUT_PULLUP);
  pinMode(minuteButton, INPUT_PULLUP);
  pinMode(commonPin, OUTPUT);
  pinMode(motorPin, OUTPUT);

Wire.begin();
Wire.beginTransmission(MPU_ADDR);
Wire.write(0x6B);
Wire.write(0);
Wire.endTransmission(true);
Serial.begin(9600);
mpu.initialize();
mpu.setFullScaleAccelRange(MPU6050_ACCEL_FS_2);
}

void loop() {
  unsigned long currentTime = millis(); // Get the current time in milliseconds
  int seconds = (currentTime / 1000) % 60; // Calculate the seconds
  int minutes = (currentTime / 60000) % 60; // Calculate the minutes
  int hours = (currentTime / 3600000) % 24; // Calculate the hours

  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_ADDR, 14, true); // request 14 bytes from MPU6050
  accelerometer_x = Wire.read() << 8 | Wire.read();
  accelerometer_y = Wire.read() << 8 | Wire.read();
  accelerometer_z = Wire.read() << 8 | Wire.read();
  int16_t temperature = Wire.read() << 8 | Wire.read();
  gyro_x = Wire.read() << 8 | Wire.read();
  gyro_y = Wire.read() << 8 | Wire.read();
  gyro_z = Wire.read() << 8 | Wire.read();

  static int16_t ax, ay, az;
  mpu.getAcceleration(&ax, &ay, &az);
  float acceleration = sqrt(ax*ax + ay*ay + az*az);
  static float lastAcceleration = acceleration;
  float accelerationDelta = abs(lastAcceleration - acceleration);
  if (accelerationDelta > 100.0) { // Change this threshold value to suit your needs
  }
  lastAcceleration = acceleration;
  delay(100);

   while (digitalRead(alarmButton) == HIGH){
    if (digitalRead(hourButton) == LOW){
      delay(500);
      alarmHours += 1;
    }
    else if (digitalRead(minuteButton) == LOW){
      delay(500);
      alarmMinutes += 1;
    }
    digitalWrite(digitPins[0], HIGH);
    displayDigit(alarmHours / 10, true);
    delay(1);
    digitalWrite(digitPins[0], LOW);

    digitalWrite(digitPins[1], HIGH);
    displayDigit(alarmHours % 10, true);
    delay(1);
    digitalWrite(digitPins[1], LOW);

    // Display the minutes
    digitalWrite(digitPins[2], HIGH);
    displayDigit(alarmMinutes / 10, true);
    delay(1);
    digitalWrite(digitPins[2], LOW);

    digitalWrite(digitPins[3], HIGH);
    displayDigit(alarmMinutes % 10, true);
    delay(1);
    digitalWrite(digitPins[3], LOW);

    if (alarmHours > 23){
      alarmHours = 0;
    }
    if (alarmMinutes == 60){
      alarmMinutes = 0;
    }
    if (digitalRead(alarmButton) == HIGH){
      break;
    }
  }

  if (digitalRead(hourButton) == LOW){
    delay(500);
    hours += 1;
  }  
  if (digitalRead(minuteButton) == LOW){
    delay(500);
    minutes += 1;
  }

  if (hours > 23){
    hours = 0;
  }
  if (minutes == 60){
    minutes = 0;
    hours += 1;
  }
  if (seconds == 60){
    seconds = 0;
    minutes += 1;
  }
  // Display the hours
  digitalWrite(digitPins[0], HIGH);
  displayDigit(hours / 10, true);
  delay(1);
  digitalWrite(digitPins[0], LOW);

  digitalWrite(digitPins[1], HIGH);
  displayDigit(hours % 10, true);
  delay(1);
  digitalWrite(digitPins[1], LOW);

  // Display the minutes
  digitalWrite(digitPins[2], HIGH);
  displayDigit(minutes / 10, true);
  delay(1);
  digitalWrite(digitPins[2], LOW);

  digitalWrite(digitPins[3], HIGH);
  displayDigit(minutes % 10, true);
  delay(1);
  digitalWrite(digitPins[3], LOW);

  analogWrite(commonPin, map(seconds, 0, 59, 0, 255));

  if (alarmHours == hours && alarmMinutes == minutes && minutes != 0){
    digitalWrite(motorPin, HIGH);
    tone(14, 1000, 100);
    delay(50);
    noTone(14);
    delay(50);

    if (accelerationDelta > 100.0){
      digitalWrite(motorPin, LOW);
      noTone(14);
    }
  }
}

void displayDigit(int digit, bool isCommonAnode) {
  // Set the segment pins based on the segment pattern for the digit
    for (int i = 0; i < 8; i++) {
    digitalWrite(segmentPins[i], bitRead(segments[digit], i));
    // Set the common anode/cathode based on the isCommonAnode flag
  if (isCommonAnode) {
    digitalWrite(digitPins[digit], HIGH);
  } else {
    digitalWrite(digitPins[digit], LOW);
  }
  }
}
